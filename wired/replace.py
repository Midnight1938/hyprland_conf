# Define the original color codes and their replacements according to the Catppuccin Macchiato color scheme

# The file path to the wired.ron file

# Function to replace colors in the file according to the mapping
def replace_colors(file_path, color_replacements):
    # Read the contents of the original file
    with open(file_path, 'r') as file:
        file_contents = file.read()
    
    # Replace all occurrences of the original color codes with the new ones
    for original_color, new_color in color_replacements.items():
        file_contents = file_contents.replace(original_color, new_color)
    
    # Write the updated contents back to the file
    with open(file_path, 'w') as file:
        file.write(file_contents)

    return f"Colors replaced in {file_path}"

# To use the function, save this script to a .py file, update the file_path variable with the correct path to your wired.ron file, and run the script.
def main():
    file_path = "wired.ron"
    color_replacements = {
        "#1D1F21": "#24273a",  # background_color
        "#66D9EF": "#363a4f",  # border_color & border_color_low
        "#403D3D": "#d20f39",  # border_color_critical
        "#661512": "#f5a97f",  # border_color_paused
        "#f8f8f2": "#cad3f5",  # color within general_size & general_notop_summary
        "#64888F": "#a5adcb",  # color within general_time & general_app_name
    }
    
    print(replace_colors(file_path, color_replacements))
    
if __name__ == "__main__":
    main()
