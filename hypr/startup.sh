#!/usr/bin/bash

# variables
config=$HOME/.config/hypr
scripts=$config/Scripts


instant=(
    "dunst"
    "swaybg -i $HOME/Pictures/wallpapers/Asthetic/BlueSkies.png"
    "wl-paste --watch cliphist store"
    "bash ~/.config/hypr/Scripts/bat_stats.sh"
    "blueberry"
    "mpd"
    "mpDris2"
    "easyeffects --gapplication-service"
   # "/usr/libexec/polkit-gnome-authentication-agent-1"
    "dbus-update-activation-environment --systemd WAYLAND_DISPLAY XDG_CURRENT_DESKTOP"
    "QT_QPA_PLATFORM=wayland"
    "MOZ_ENABLE_WAYLAND=1"
)

parallel --jobs 7 ::: "${instant[@]}" &

# notification daemon
#!# dunst &

# wallpaper
#!# swaybg -i $HOME/Pictures/wallpapers/Asthetic/BlueSkies.png &

##mpvpaper -o "no-audio --loop-playlist shuffle" "*" ~/Pictures/wallpapers/Video/White-Oak-Official-Chillhop-Music-wallpaper.mp4 &

#setxkbmap -variant colemak -option rupeesign:4 & 
# effects
##copyq &
#wl-paste --watch  clipit store &
#wl-paste -t text --watch clipman store &

#!# wl-paste --watch ~/go/bin/cliphist store &

## redshift-gtk &
## systemctl start --user emacs &
#!# bash ~/.config/hypr/Scripts/bat_stats.sh &
#!# blueman-applet &
#!# mpd & mpDris2 & easyeffects --gapplication-service &

# other
/usr/libexec/polkit-gnome-authentication-agent-1 &
#!# dbus-update-activation-environment --systemd WAYLAND_DISPLAY XDG_CURRENT_DESKTOP &
#!# QT_QPA_PLATFORM=wayland & MOZ_ENABLE_WAYLAND=1 &
