
# For changing configs based on 
# number of connected monitors 

if [[ $(xrandr | awk '/ connected/ {count++} END {print count}') = 1 ]]; then
    hyprctl keyword monitor eDP-1, highres@highrr, 0x0, 1 
    echo "single"
else
    hyprctl keyword monitor eDP-1, highres@highrr, 1920x0, 1
    hyprctl keyword monitor HDMI-A-1, highres@highrr, 0x0, 1 
    echo "dual"
fi
