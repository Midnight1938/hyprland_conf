#!/bin/bash

while true
do 
battery_level=$(acpi | grep -P -o '[0-9]+(?=%)')
ac_power=$(acpi | grep -c "Charging")
battery_full=$(acpi | grep -c "Full")

if [[ $ac_power -eq 1 && $battery_level -eq 100 || $battery_full -eq 1 ]]; then
    # it notifies  Battery is full and shows the battery
    # full image which is stored in /usr/local/bin directory
    
    # so, it will narrate, please remove the
    # charger. It's charged up to 100%.
    notify-send -u critical "Oii the Battery is $battery_level%"
    play ~/.config/hypr/Scripts/sounds/abe_yaar.mp3
    curl \
        -H "Title: Battery Full!"\
        -H "Priority: default"\
        -H "Tags: loudspeaker"\
        -d "Battery is at $(acpi | grep -P -o '[0-9]+(?=%)'). Disconnect Charger."\
        ntfy.sh/skshm_batt_stat
    

# when the battery is not charging
# and it goes below 25%
elif [[ $ac_power -eq 0 ]] && [[ "$battery_level" -lt 25 ]]; then
    #"please connect the charger"
    echo $battery_level
    notify-send -u critical "M8, Battery is Low." "Level: $battery_level% "
    play ~/.config/hypr/Scripts/sounds/Oh_No.mp3
    curl \
        -H "Title: Low Battery!"\
        -H "Priority: high"\
        -H "Tags: rotating_light"\
        -d "Battery is at $(acpi | grep -P -o '[0-9]+(?=%)'). Connect Charger."\
        ntfy.sh/skshm_batt_stat

fi
sleep 300
done
