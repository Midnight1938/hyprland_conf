import unicodedata

character = '❤'  # Replace this with the character or icon you want to find the Unicode of

unicode_codepoint = ord(unicodedata.lookup('NAME OF CHARACTER'))
print("Unicode codepoint:", unicode_codepoint)
